package com.exam.resources.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.exam.model.mapping.WeatherGroupResponse;
import com.exam.service.WeatherService;

@RestController
public class WeatherResource {
	
	@Autowired
	private WeatherService weatherService;
	
	@GetMapping("/weather/group/default")
	public WeatherGroupResponse getWeatherGroup() {	
		WeatherGroupResponse weatherGroup = weatherService.getDefaultGroup();
		return weatherGroup;
	}
	
	@PostMapping("weather/persist/latest")
	public String saveLatest() {
		WeatherGroupResponse weatherGroupResponse = weatherService.getDefaultGroup();
		weatherService.insert(weatherGroupResponse);
		return "{\"message\": \"Successfully saved latest weather report to database.\" }";
	}
	
//	@GetMapping("/weather/test")
//	public String getTestString() {	
//		return "{ \"message\": \"Hello World!\"}";
//	}
	
}
