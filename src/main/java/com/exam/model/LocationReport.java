package com.exam.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class LocationReport {
	private String location;
	private String actualWeather;
	private String temperature;

	@Id
	@GeneratedValue
	private long id;

	@ManyToOne
	private WeatherLog report;

	public LocationReport() {
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getActualWeather() {
		return actualWeather;
	}

	public void setActualWeather(String actualWeather) {
		this.actualWeather = actualWeather;
	}

	public String getTemperature() {
		return temperature;
	}

	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}

	public WeatherLog getReport() {
		return report;
	}

	public void setReport(WeatherLog report) {
		this.report = report;
	}

}
