package com.exam.model.mapping;

import java.util.List;

import com.exam.pojo.response.WeatherGroupDTO;
import com.fasterxml.jackson.annotation.JsonProperty;

public class WeatherGroupResponse {
	
	@JsonProperty("locations")
	private List<LocationWeather> locationWeatherList;

	
	public WeatherGroupResponse() {}
	
	public WeatherGroupResponse(WeatherGroupDTO weatherGroupDTO) {
	}

	public List<LocationWeather> getLocationWeatherList() {
		return locationWeatherList;
	}

	public void setLocationWeatherList(List<LocationWeather> locationWeatherList) {
		this.locationWeatherList = locationWeatherList;
	}


}

