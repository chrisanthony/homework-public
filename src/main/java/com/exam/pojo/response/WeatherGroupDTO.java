package com.exam.pojo.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherGroupDTO{
	private int cnt;
	private List<LocationWeatherDTO> list;
	
	public WeatherGroupDTO() {}
	
	public int getCnt() {
		return cnt;
	}
	public void setCnt(int cnt) {
		this.cnt = cnt;
	}
	public List<LocationWeatherDTO> getList() {
		return list;
	}
	public void setList(List<LocationWeatherDTO> list) {
		this.list = list;
	}
}
