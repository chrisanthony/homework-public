package com.exam.util;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StringUtils {
	public static List<Integer> getIdsFromString(String ids) {
		return Arrays.stream(ids.split(","))
				.map(Integer::parseInt)
				.collect(Collectors.toList());
	}
}
