package com.exam.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.exam.model.WeatherLog;

@Repository
public interface WeatherRepository extends JpaRepository<WeatherLog, Long>{
	
	WeatherLog findFirstByOrderByIdAsc();
}
