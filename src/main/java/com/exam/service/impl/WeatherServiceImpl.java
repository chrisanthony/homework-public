package com.exam.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.exam.model.LocationReport;
import com.exam.model.WeatherLog;
import com.exam.model.mapping.LocationWeather;
import com.exam.model.mapping.WeatherGroupResponse;
import com.exam.pojo.response.LocationWeatherDTO;
import com.exam.pojo.response.WeatherGroupDTO;
import com.exam.repository.WeatherRepository;
import com.exam.service.WeatherService;

@Service
public class WeatherServiceImpl implements WeatherService {

	@Autowired
	private Environment env;
	
	@Autowired
	private WeatherRepository weatherRepository;
	
	private final RestTemplate restTemplate;
	
	public WeatherServiceImpl(RestTemplateBuilder restTemplateBuilder) {
		restTemplate = restTemplateBuilder.build();
	}

	@Override
	public long countRows() {
		return weatherRepository.count();
	}

	@Override
	public void insert(WeatherGroupResponse weatherGroupResponse) {
		WeatherLog weatherLog = new WeatherLog();
		weatherLog.setLocations(getLocationReports(weatherGroupResponse));
		weatherRepository.save(weatherLog);
		if(countRows()>5) {
			WeatherLog oldest = weatherRepository.findFirstByOrderByIdAsc();
			weatherRepository.delete(oldest);
		}
	}

	@Override
	public WeatherGroupResponse getDefaultGroup() {
		WeatherGroupDTO groupDTO = restTemplate.getForObject(assembleDefaultUrl(), WeatherGroupDTO.class);
		
		WeatherGroupResponse groupResponse = assembleWeatherResponse(groupDTO);
		return groupResponse;
	}

	private String assembleDefaultUrl() {
		String idGroup = env.getProperty("london.weather.id") 
				+ "," + env.getProperty("prague.weather.id") + ","
				+ env.getProperty("sanfrancisco.weather.id");
		String host = env.getProperty("weather.api.host");
		String path = env.getProperty("weather.api.group.path");

		String url = "http://" 
					+ host + path 
					+ "?id=" + idGroup 
					+ "&APPID=" + env.getProperty("weather.api.appid");
		return url;
	}
	
	private WeatherGroupResponse assembleWeatherResponse(WeatherGroupDTO weatherGroupDTO) {
		WeatherGroupResponse groupResponse = new WeatherGroupResponse();
		List <LocationWeather> locationList = new ArrayList<LocationWeather>();
		for(LocationWeatherDTO weatherDTO: weatherGroupDTO.getList()) {
			LocationWeather location = new LocationWeather();
			location.setLocation(weatherDTO.getName());
			location.setActualWeather(weatherDTO.getWeather().get(0).getDescription());
			location.setTemperature(String.valueOf(weatherDTO.getMain().getTemp()));
			locationList.add(location);
		}
		groupResponse.setLocationWeatherList(locationList);
		return groupResponse;
	}
	
	private List<LocationReport> getLocationReports(WeatherGroupResponse weatherGroupResponse) {
		List <LocationReport> locationReports = new ArrayList<LocationReport>();
		for (  LocationWeather locWeather: weatherGroupResponse.getLocationWeatherList()) {
			LocationReport locationReport = new LocationReport();
			locationReport.setLocation(locWeather.getLocation());
			locationReport.setActualWeather(locWeather.getActualWeather());
			locationReport.setTemperature(locWeather.getTemperature());
			locationReports.add(locationReport);
		}
		return locationReports;
	}
	


}
