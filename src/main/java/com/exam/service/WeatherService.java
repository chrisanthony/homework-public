package com.exam.service;

import org.springframework.stereotype.Service;

import com.exam.model.mapping.WeatherGroupResponse;

@Service
public interface WeatherService {
	long countRows();
	void insert(WeatherGroupResponse weatherGroupResponse);
	WeatherGroupResponse getDefaultGroup();
}
