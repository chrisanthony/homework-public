#Weather Java Exam

#use port 8084

#rest api
#requirement 1:  API that uses list of city ids separated by comma to display weather

GET localhost:8084/weather/group/default

#requirement 2:  API that uses result of API #1 and saves to database, keeping records #up to maximum of 5 latest

GET localhost:8084/weather/persist/latest

#db h2
http://localhost:8084/h2/login.jsp